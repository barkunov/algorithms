import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadTest {
    public static void main(String[] args) throws InterruptedException {
        System.out.println("start");
        Thread thread = new Thread(() -> {
            try {
                while (true) {
                    Thread.sleep(1000L);
                    System.out.println("boom...");
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, "kirill-thread");

        thread.start();
        System.out.println("wait");
        try {
            Thread.sleep(60000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("end");
    }
}


class NoThreadTest {
    public static void main(String[] args) {
        System.out.println("start");
        try {
            while (true) {
                Thread.sleep(1000L);
                System.out.println("boom...");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("end");
    }
}

class ExecutorTest {
    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(3);
        executorService.submit(() -> {
            try {
                while (true) {
                    Thread.sleep(1000L);
                    System.out.println("boom...");
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        executorService.submit(() -> {
            try {
                while (true) {
                    Thread.sleep(2000L);
                    System.out.println("tram...");
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        try {
            Thread.sleep(60000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        executorService.shutdownNow();
        System.out.println("end");
    }
}
