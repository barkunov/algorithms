package quiz;

import java.util.Arrays;

/**
 * @author Kirill Barkunov on 5.11.17.
 */
public class PaintHouses {

    // input
    private static int[][] prices = new int[][]{{1, 2, 3}, {1, 5, 6}, {7, 8, 9}};

    ////////////////////////////////////////////////////
    private static int resultPrice = Integer.MAX_VALUE;
    private static int[] result;

    private static int len = prices.length;
    private static int width = prices[0].length;


    public static void main(String[] args) {

        paintStreet(0, new int[len], -1);

        System.out.println(Arrays.toString(result));
        System.out.println(resultPrice);

    }

    private static void paintStreet(int index, int[] res, int prevColor) {
        if (index < len) {
            for (int i = 0; i < width; i++) {
                if (i != prevColor) {
                    int[] newRes = res.clone();
                    newRes[index] = i;
                    paintStreet(index + 1, newRes, i);
                }
            }
        } else {
            int price = countPrice(res);

            if (price < resultPrice) {
                resultPrice = price;
                result = res;
            }
        }
    }

    private static int countPrice(int[] res) {
        int price = 0;
        for (int i = 0; i < len; i++) {
            price += prices[i][res[i]];
        }

        return price;
    }
}
