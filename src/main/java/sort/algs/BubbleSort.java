package sort.algs;

import sort.Sort;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Kirill Barkunov on 26.10.17.
 */
public class BubbleSort<T extends Comparable<T>> implements Sort<T> {

    @Override
    public List<T> sort(List<T> list) {
        list = new ArrayList<>(list);

        for (int i = 0; i < list.size(); i++) {
            for (int k = i; k < list.size(); k++) {
                if (list.get(i).compareTo(list.get(k)) > 0) {
                    T temp = list.get(i);
                    list.set(i, list.get(k));
                    list.set(k, temp);
                }
            }
        }

        return list;
    }
}
