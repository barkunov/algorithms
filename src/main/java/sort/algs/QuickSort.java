package sort.algs;

import sort.Sort;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author Kirill Barkunov on 26.10.17.
 */
public class QuickSort<T extends Comparable<T>> implements Sort<T> {
    @Override
    public List<T> sort(List<T> list) {
        list = new ArrayList<T>(list);

        sortFinal(list, 0, list.size() - 1);

        return list;
    }

    private void sortFinal(List<T> list, int left, int right) {
        int pivot = partitionFromRight(list, left, right);
//        int pivot = partitionHard(list, left, right);

        sortFinal(list, 0, pivot - 1);
        sortFinal(list, pivot, list.size() - 1);
    }


    public int partitionVaniuchayaSranayaZalupaGovno(List<T> list, int left, int right) {
        T base = list.get(left);

        int i = left + 1;
        int k = right;

        while (i <= k) {

            while (i <= k
                    && list.get(i).compareTo(base) < 0) {
                i++;
            }

            while (i < k
                    && list.get(k).compareTo(base) >= 0) {
                k++;
            }

            Collections.swap(list, i, k);
            i++;
            k++;


        }
        return -1;

    }

    public int partitionFromRight(List<T> arr, int low, int high) {
        // pivot (Element to be placed at right position)
        T pivot = arr.get(high);

        int i = (low - 1);  // Index of smaller element

        for (int j = low; j <= high - 1; j++) {
            // If current element is smaller than or
            // equal to pivot
            if (arr.get(j).compareTo(pivot) <= 0) {
                i++;    // increment index of smaller element
                swapElements(arr, i, j);
            }
        }
        swapElements(arr, i + 1, high);
        return (i + 1);
    }

    public int partitionHard(List<T> list, int left, int right) {
        T pivotValue = list.get(left);
        List<T> leftByPivot = new ArrayList<>();
        List<T> rightByPivot = new ArrayList<>();
        for (int i = left; i <= right; i++) {
            if (list.get(i).compareTo(pivotValue) >= 0) {
                rightByPivot.add(list.get(i));
            } else {
                leftByPivot.add(list.get(i));
            }
        }

        int index = left;
        for (int i = 0; i < leftByPivot.size(); i++) {
            list.set(index, leftByPivot.get(i));
            leftByPivot.remove(i);
            index++;
        }

        int newPivot = index;
        for (int i = 0; i < rightByPivot.size(); i++) {
            list.set(index, rightByPivot.get(i));
            rightByPivot.remove(i);
            index++;
        }

        return newPivot;
    }

    public int partition(List<T> list, int left, int right) {
        int pivot = left;

        left++;

        while (true) {
            while (left < right && list.get(left).compareTo(list.get(pivot)) <= 0) {
                left++;
            }
            while (right > left && list.get(right).compareTo(list.get(pivot)) > 0) {
                right--;
            }

            if (left == right) {
                break;
            } else {
                swapElements(list, left, right);
            }
        }

        swapElements(list, pivot, left);
        return left;
    }

    private void sort2(List<T> list, int start, int end) {
//        System.out.println(start + "-" + end);
        if (start >= end) {
            return;
        }

        if (start == end - 1) {
            if (list.get(start).compareTo(list.get(end)) > 0) {
                swapElements(list, start, end);
            }
            return;
        }

        int medium = start + ((end - start) / 2);
        T root = list.get(medium);

        for (int i = medium - 1; i >= 0; i--) {
            if (list.get(i).compareTo(root) > 0) {
                list.add(end, list.get(i));
                list.remove(i);
            }
        }

        for (int i = medium + 1; i < end + 1; i++) {
            if (list.get(i).compareTo(root) < 0) {
                list.add(start, list.get(i));
                list.remove(i);
            }
        }

        sort2(list, start, medium);
        sort2(list, medium + 1, end);
    }

    private void sort(List<T> list, int start, int end) {

        if (start >= end) {
            return;
        }

        if (start == end - 1) {
            if (list.get(start).compareTo(list.get(end)) > 0) {
                swapElements(list, start, end);
            }
            return;
        }

        T root = list.get(start);
        int originStart = start;
        int originEnd = end;
        start++;

        while (start < end) {
            if ((list.get(start).compareTo(root) >= 0)
                    && (list.get(end).compareTo(root) <= 0)) {
                swapElements(list, start, end);
                start++;
                end--;
            } else if (list.get(start).compareTo(root) < 0) {
                start++;
            } else if (list.get(end).compareTo(root) > 0) {
                end--;
            }
        }

        System.out.println("Before moving pivot: " + list);
        System.out.println("Start - End" + start + "-" + end);
        if (start != end) {
            System.err.println(start + " - " + end);
        }


        list.add(start + 1, list.get(originStart));
        list.remove(originStart);

        sort(list, originStart, start);
        sort(list, start, originEnd);
    }

    private void sort3(List<T> list, int start, int end) {

        if (start >= end) {
            return;
        }

        if (start == end - 1) {
            if (list.get(start).compareTo(list.get(end)) > 0) {
                swapElements(list, start, end);
            }
            return;
        }

        T pivot = list.get(start);
        int pivotIndex = start;
        int originStart = start;
        int originEnd = end;
        start++;

//        while (list.get(start).compareTo(pivot) <= 0) {
        while (start > end) {
            if (list.get(start).compareTo(pivot) <= 0) {
                while (start > end) {
                    if (list.get(end).compareTo(pivot) >= 0) {
                        swapElements(list, start, end);
                    }
                    end++;
                }
            }
            start++;
        }

        swapElements(list, pivotIndex, start);

        System.out.println("Before moving pivot: " + list);
        System.out.println("Start - End" + start + "-" + end);
        if (start != end) {
            System.err.println(start + " - " + end);
        }


//        list.add(start + 1, list.get(originStart));
//        list.remove(originStart);

        sort(list, originStart, start);
        sort(list, start, originEnd);
    }

    private void swapElements(List<T> list, int start, int end) {
        T temp = list.get(start);
        list.set(start, list.get(end));
        list.set(end, temp);
    }

    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(5, 4, 3, 1, 10, 2, 6, 7, 7, 5, 34, 2, 3, 5, 6, 7, 6, 4, 6);
//        List<Integer> list = Arrays.asList(2, 2, 1);
        System.out.println(list);
        int pivot = new QuickSort<Integer>().partitionFromRight(list, 0, list.size() - 1);
        System.out.println(list);
        System.out.println("pivot: " + pivot);

//        divide(0, 18);
    }

    private static void divide(int start, int end) {
        if (start >= end) {
//            System.out.println("start greater");
            return;
        }

        if (start == end - 1) {
//            System.out.println("2 elements");
            return;
        }
        System.out.println(start + "-" + end);
        int medium = start + ((end - start) / 2);
        divide(start, medium);
        divide(medium + 1, end);
    }
}
