package sort.algs;

import sort.Sort;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Kirill Barkunov on 26.10.17.
 */
public class InsertionSort<T extends Comparable<T>> implements Sort<T> {
    @Override
    public List<T> sort(List<T> list) {
        List<T> result = new ArrayList<>(list.size());

        for (T el : list) {
            insert(result, el);
        }

        return result;
    }

    private void insert(List<T> list, T el) {
        if (list.size() == 0) {
            list.add(el);
            return;
        }

        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).compareTo(el) >= 0) {
                list.add(i, el);
                return;
            }
        }

        list.add(el);
    }
}
