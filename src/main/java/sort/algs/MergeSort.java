package sort.algs;

import sort.Sort;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Kirill Barkunov on 26.10.17.
 */
public class MergeSort<T extends Comparable<T>> implements Sort<T> {

    @Override
    public List<T> sort(List<T> list) {
        if (list == null || list.size() == 0) {
            return new ArrayList<>();
        }

        if (list.size() == 1) {
            return list;
        }

        list = new ArrayList<>(list);

        int center = list.size() / 2;

        List<T> left = new ArrayList<>(list.subList(0, center));
        List<T> right = new ArrayList<>(list.subList(center, list.size()));

        left = sort(left);
        right = sort(right);

        return merge(left, right);
    }

    private List<T> merge(List<T> left, List<T> right) {
        List<T> result = new ArrayList<>(left.size() + right.size());

        while (left.size() != 0 || right.size() != 0) {
            if (left.size() == 0) {
                result.addAll(right);
                right = new ArrayList<>(0);
            } else if (right.size() == 0) {
                result.addAll(left);
                left = new ArrayList<>(0);
            } else {
                if (left.get(0).compareTo(right.get(0)) <= 0) {
                    result.add(left.get(0));
                    left.remove(0);
                } else {
                    result.add(right.get(0));
                    right.remove(0);
                }
            }
        }

        return result;
    }
}
