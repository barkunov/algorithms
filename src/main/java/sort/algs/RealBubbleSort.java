package sort.algs;

import sort.Sort;

import java.util.ArrayList;
import java.util.List;

public class RealBubbleSort<T extends Comparable<T>> implements Sort<T> {
    @Override
    public List<T> sort(List<T> list) {
        list = new ArrayList<>(list);
        for (int i = 0; i < list.size() - 1; i++) {
            int listSize = list.size() - i;
            for (int j = 0; j < (listSize - 1); j++) {
                if (list.get(j).compareTo(list.get(j + 1)) > 0) {
                    T temp = list.get(j);
                    list.set(j, list.get(j + 1));
                    list.set(j + 1, temp);
                }
            }
        }
        return list;
    }
}
