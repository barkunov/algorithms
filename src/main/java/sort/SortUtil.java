package sort;

import sort.algs.MergeSort;

import java.util.List;

/**
 * @author Kirill Barkunov on 26.10.17.
 */
public class SortUtil {

    public static<T extends Comparable<T>> List<T> mergeSort(List<T> list) {
        return new MergeSort<T>().sort(list);
    }
}
