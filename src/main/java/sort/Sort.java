package sort;

import java.util.List;

/**
 * @author Kirill Barkunov on 26.10.17.
 */
public interface Sort<T extends Comparable<T>> {
    List<T> sort(List<T> list);
}
