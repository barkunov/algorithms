package algo;

import java.util.List;

class MaxSpan {

    static Integer maxSpan(List<Integer> list) {
        int maxSpan = 1;
        for (int i = 0; i < list.size(); i++) {
            Integer currentElement = list.get(i);
            int tempSpan = list.lastIndexOf(currentElement) - list.indexOf(currentElement) + 1;
            if (tempSpan > maxSpan) {
                maxSpan = tempSpan;
            }
        }
        return maxSpan;
    }
}
