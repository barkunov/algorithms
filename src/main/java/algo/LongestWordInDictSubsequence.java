package algo;

import com.google.common.collect.Sets;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

/**
 * @url https://techdevguide.withgoogle.com/paths/foundational/find-longest-word-in-dictionary-that-subsequence-of-given-string#code-challenge
 */

class LongestWordInDictSubsequence {
    static String calcBruteForce(String baseWord, List<String> words) {
        HashSet<String> allSubWords = buildSubWordsDict(baseWord);

        String longest = null;
        int longestLength = -1;
        for (String word : words) {
            if (allSubWords.contains(word) && word.length() > longestLength) {
                longest = word;
                longestLength = longest.length();
            }
        }
        return longest;
    }

    private static HashSet<String> buildSubWordsDict(String word) {
        if (word == null) {
            return null;
        }
        if (word.length() == 1) {
            return Sets.newHashSet(word);
        }

        HashSet<String> temp = Sets.newHashSet(word);
        for (int i = 0; i < word.length(); i++) {
            temp.addAll(buildSubWordsDict(word.substring(0, i) + word.substring(i + 1)));
        }
        return temp;
    }


    //////////////////
    //////////////////
    //////////////////

    static String calcIterative(String baseWord, List<String> words) {
        String longest = null;
        int longestLength = -1;

        for (String word : words) {
            if (word.length() > longestLength && isSubsequenceStraight(baseWord, word)) {
                longest = word;
                longestLength = longest.length();
            }
        }
        return longest;
    }

    static String calcIterativeWithIndexesMap(String baseWord, List<String> words) {
        String longest = null;
        int longestLength = -1;

        HashMap<Character, LinkedList<Integer>> baseWordMap = buildCharMap(baseWord);

        for (String word : words) {
            if (word.length() > longestLength && isSubsequence(baseWordMap, word)) {
                longest = word;
                longestLength = longest.length();
            }
        }
        return longest;
    }

    private static boolean isSubsequence(HashMap<Character, LinkedList<Integer>> baseWordDict, String word) {
        int currentIndex = -1;
        for (int i = 0; i < word.length(); i++) {
            Character c = word.charAt(i);

            int finalCurrentIndex = currentIndex;
            int searchIndex = baseWordDict.computeIfAbsent(c, ignore -> new LinkedList<>())
                    .stream()
                    .mapToInt(v -> v)
                    .filter(v -> v > finalCurrentIndex)
                    .findFirst()
                    .orElse(-2);

            if (searchIndex < currentIndex) {
                return false;
            } else {
                currentIndex = searchIndex;
            }
        }
        return true;
    }

    private static HashMap<Character, LinkedList<Integer>> buildCharMap(String baseWord) {
        HashMap<Character, LinkedList<Integer>> dict = new HashMap<>();
        for (int i = 0; i < baseWord.length(); i++) {
            dict.computeIfAbsent(baseWord.charAt(i), ignore -> new LinkedList<>())
                    .addLast(i);
        }
        return dict;
    }

    static boolean isSubsequenceStraight(String baseWord, String word) {
        int matchingIndex = -1;
        for (int i = 0; i < word.length(); i++) {
            if (matchingIndex < baseWord.length()) {
                baseWord = baseWord.substring(matchingIndex + 1);
            } else {
                return false;
            }
            matchingIndex = baseWord.indexOf(word.charAt(i));
            if (matchingIndex == -1) {
                return false;
            }
        }

        return true;
    }
}
