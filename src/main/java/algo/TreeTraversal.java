package algo;

import lib_new.BTree;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

class TreeTraversal {
    static <T extends Comparable<T>> List<BTree.Node<T>> breadthFirstTraverse(BTree<T> tree) {
        Queue<BTree.Node<T>> queue = new LinkedList<>();
        LinkedList<BTree.Node<T>> result = new LinkedList<>();

        queue.add(tree.getTop());
        while (!queue.isEmpty()) {
            BTree.Node<T> node = queue.poll();
            result.add(node);

            if (node.left != null) {
                queue.add(node.left);
            }
            if (node.right != null) {
                queue.add(node.right);
            }
        }
        return result;
    }

    static <T extends Comparable<T>> List<BTree.Node<T>> depthFirstTraverse(BTree<T> tree) {
        Stack<BTree.Node<T>> stack = new Stack<>();
        LinkedList<BTree.Node<T>> result = new LinkedList<>();

        stack.add(tree.getTop());
        while (!stack.isEmpty()) {
            BTree.Node<T> node = stack.pop();
            result.add(node);

            if (node.left != null) {
                stack.add(node.left);
            }
            if (node.right != null) {
                stack.add(node.right);
            }
        }
        return result;
    }
}
