package algo;

import java.util.List;
import java.util.stream.IntStream;

public class StringZip {
    static String unzip(String zip) {
        ZippedString zippedString = ZippedString.of(zip);
        return zip;
    }


    private static class ZippedString {
        private final List<ZippedStringElement> elements;

        private static ZippedString of(String zip) {
            for (int i = 0; i < zip.length(); i++) {
                char c = zip.charAt(i);
                if (Character.isDigit(c)) {

                }
            }
            return null;
        }

        private ZippedString(List<ZippedStringElement> elements) {
            this.elements = elements;
        }

        private String buildString() {
            StringBuilder sb = new StringBuilder();
            for (ZippedStringElement el: elements) {
                sb.append(el.buildString());
            }
            return sb.toString();
        }


        private interface ZippedStringElement {
            String buildString();
        }

        private static class ZippedStringElementString implements ZippedStringElement {
            private final int count;
            private final String value;

            private ZippedStringElementString(int count, String value) {
                this.count = count;
                this.value = value;
            }

            @Override
            public String buildString() {
                StringBuilder sb = new StringBuilder();
                IntStream.rangeClosed(1, count).forEach(ignore -> sb.append(value));
                return sb.toString();
            }
        }

        private static class ZippedStringElementNested implements ZippedStringElement{
            private final int count;
            private final ZippedStringElement nested;

            private ZippedStringElementNested(int count, ZippedStringElement nested) {
                this.count = count;
                this.nested = nested;
            }

            @Override
            public String buildString() {
                StringBuilder sb = new StringBuilder();
                IntStream.rangeClosed(1, count).forEach(ignore -> sb.append(nested.buildString()));
                return sb.toString();
            }
        }
    }
}
