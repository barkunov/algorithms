package algo;

import java.util.Collections;
import java.util.List;

public class CanBalance {
    static boolean canBalance(List<Integer> list) {
        int sum = list.stream().mapToInt(i -> i).sum();
        if (sum % 2 != 0) {
            return false;
        }

        list.sort(Integer::compareTo);

        int expectedHalfSum = sum / 2;
        int actualHalfSum = 0;
        for (Integer el: list) {
            if (actualHalfSum == expectedHalfSum) {
                return true;
            } if (actualHalfSum > expectedHalfSum) {
                return false;
            } else {
                actualHalfSum += el;
            }
        }
        return false;
    }


    static boolean canBalanceVano(List<Integer> list) {
        int sum = list.stream().mapToInt(i -> i).sum();
        if (sum % 2 != 0) {
            return false;
        }

        list.sort(Collections.reverseOrder());

        int expectedHalfSum = sum / 2;
        int actualHalfSum = 0;
        for (Integer el: list) {
            if (actualHalfSum + el == expectedHalfSum) {
                return true;
            } if (actualHalfSum + el < expectedHalfSum) {
                actualHalfSum += el;
            }
        }
        return actualHalfSum == expectedHalfSum;
    }
}
