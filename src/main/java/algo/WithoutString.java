package algo;

class WithoutString {
    static String withoutString(String base, String remove) {
        return base.replace(remove, "");
    }

    static String withoutStringManual(String base, String remove) {
        int searchIndex = base.indexOf(remove);
        while (searchIndex != -1) {
            base = base.substring(0, searchIndex) + base.substring(searchIndex + remove.length());
            searchIndex = base.indexOf(remove);
        }
        return base;
    }
}
