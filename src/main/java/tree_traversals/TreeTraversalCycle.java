package tree_traversals;

import lib.model.TreeNode;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * @author Kirill Barkunov on 7.12.18.
 */
class TreeTraversalCycle {

    static List<Integer> traverseDepthFirstPreOrderCycle(TreeNode<Integer> node) {
        ArrayList<Integer> depthFirstInOrder = new ArrayList<>();
        Stack<TreeNode<Integer>> stack = new Stack<>();
        stack.add(node);

        while (!stack.isEmpty()) {
            TreeNode<Integer> currentNode = stack.pop();
            depthFirstInOrder.add(currentNode.getValue());
            if (currentNode.getRight() != null) {
                stack.add(currentNode.getRight());
            }
            if (currentNode.getLeft() != null) {
                stack.add(currentNode.getLeft());
            }
        }

        return depthFirstInOrder;
    }

//    static List<Integer> traverseDepthFirstPreOrderCycle2(TreeNode<Integer> node) {
//        ArrayList<Integer> depthFirstInOrder = new ArrayList<>();
//        Stack<TreeNode<Integer>> stack = new Stack<>();
//
//        TreeNode<Integer> current = node;
//        while (current != null || !stack.isEmpty()) {
//            while (current != null) {
//                stack.add(current);
//                depthFirstInOrder.add(current.buildString());
//                current = current.getLeft();
//            }
//            if (!stack.isEmpty()) {
//                TreeNode<Integer> pop = stack.pop();
//                current = pop.getRight();
//            }
//        }
//
//        return depthFirstInOrder;
//    }

    static List<Integer> traverseDepthFirstInOrderCycle(TreeNode<Integer> node) {
        ArrayList<Integer> depthFirstInOrder = new ArrayList<>();
        Stack<TreeNode<Integer>> stack = new Stack<>();

        TreeNode<Integer> current = node;
        while (current != null || !stack.isEmpty()) {
            while (current != null) {
                stack.add(current);
                current = current.getLeft();
            }
            if (!stack.isEmpty()) {
                TreeNode<Integer> pop = stack.pop();
                depthFirstInOrder.add(pop.getValue());
                current = pop.getRight();
            }
        }

        return depthFirstInOrder;
    }

    static List<Integer> traverseBreadthFirstCycle(TreeNode<Integer> node) {
        ArrayList<Integer> breadthFirstOrder = new ArrayList<>();
        Queue<TreeNode<Integer>> queue = new LinkedBlockingQueue<>();
        queue.add(node);

        while (!queue.isEmpty()) {
            TreeNode<Integer> currentNode = queue.poll();
            breadthFirstOrder.add(currentNode.getValue());
            if (currentNode.getLeft() != null) {
                queue.add(currentNode.getLeft());
            }
            if (currentNode.getRight() != null) {
                queue.add(currentNode.getRight());
            }
        }


        return breadthFirstOrder;
    }
}
