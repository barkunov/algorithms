package tree_traversals;

import lib.model.SimpleTreeNode;
import lib.model.TreeNode;

public class TreeUtils {
    public static void addNode(TreeNode<Integer> node, int i) {
        if (i > node.getValue()) {
            if (node.getRight() != null) {
                addNode(node.getRight(), i);
            } else {
                TreeNode<Integer> newLeaf = new SimpleTreeNode<>(i);
                node.setRight(newLeaf);
            }
        } else if (i < node.getValue()) {
            if (node.getLeft() != null) {
                addNode(node.getLeft(), i);
            } else {
                TreeNode<Integer> newLeaf = new SimpleTreeNode<>(i);
                node.setLeft(newLeaf);
            }
        }
    }
}
