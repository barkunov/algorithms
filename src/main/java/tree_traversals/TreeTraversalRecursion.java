package tree_traversals;

import lib.model.SimpleTreeNode;

import java.util.List;

public class TreeTraversalRecursion {

    public static List<Integer> traverseDepthFirstInRecursion(List<Integer> order, SimpleTreeNode<Integer> node) {
        if (node == null) return null;

        traverseDepthFirstInRecursion(order, node.getLeft());
        order.add(node.getValue());
        traverseDepthFirstInRecursion(order, node.getRight());

        return order;
    }

    public static List<Integer> traverseDepthFirstPreOrderRecursion(List<Integer> order, SimpleTreeNode<Integer> node) {
        if (node == null) return null;

        order.add(node.getValue());
        traverseDepthFirstPreOrderRecursion(order, node.getLeft());
        traverseDepthFirstPreOrderRecursion(order, node.getRight());

        return order;
    }

}
