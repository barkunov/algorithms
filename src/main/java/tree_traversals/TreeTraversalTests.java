package tree_traversals;

import lib.model.SimpleTreeNode;

import java.util.ArrayList;
import java.util.List;

import static tree_traversals.TreeTraversalCycle.*;
import static tree_traversals.TreeTraversalRecursion.traverseDepthFirstInRecursion;
import static tree_traversals.TreeTraversalRecursion.traverseDepthFirstPreOrderRecursion;
import static tree_traversals.TreeUtils.addNode;

public class TreeTraversalTests {
    public static void main(String[] args) {
        SimpleTreeNode<Integer> root = new SimpleTreeNode<>(10);
        addNode(root, 20);
        addNode(root, 30);
        addNode(root, 15);
        addNode(root, 17);
        addNode(root, 5);
        addNode(root, 7);
        addNode(root, 3);
        addNode(root, 1);

        List<Integer> ordered = traverseBreadthFirstCycle(root);
        System.out.println(ordered.toString() + " - Breadth first cycle");

        ordered = traverseDepthFirstInOrderCycle(root);
        System.out.println(ordered.toString() + " - Depth first in order cycle");

        ordered = traverseDepthFirstInRecursion(new ArrayList<>(), root);
        System.out.println(ordered.toString() + " - Depth first in order recursion");

        ordered = traverseDepthFirstPreOrderCycle(root);
        System.out.println(ordered.toString() + " - Depth first pre order cycle");

        ordered = traverseDepthFirstPreOrderRecursion(new ArrayList<>(), root);
        System.out.println(ordered.toString() + " - Depth first pre order recursion");

        System.out.println("done");
        System.exit(0);
    }
}
