package spell_check;

import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;

public class SpellChecker {
    String url = "https://avidreaders.ru/download/anna-karenina.html?f=txt";

    final Map<String, Integer> dict = new HashMap<>();
    final long totalCounter;
    final String alphabet;

    public SpellChecker(Lang lang) {
        switch (lang) {
            case RU:
                initRu();
                alphabet = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
                break;
            case EN:
            default:
                initEn();
                alphabet = "abcdefghijklmnopqrstuvwxyz";
                break;
        }
        totalCounter = dict.values().stream().reduce(Integer::sum).orElse(0);
    }

    private void initRu() {
        InputStream in = this.getClass().getResourceAsStream("/ru.txt");
        Scanner scanner = new Scanner(in);
        System.out.println("Словарь загружается...");
        while (scanner.hasNext()) {
            for (String word : scanner.nextLine().toLowerCase().split("(?U)\\W+")) {
                dict.putIfAbsent(word, 1);
                dict.put(word, dict.get(word) + 1);
            }
        }
    }

    private void initEn() {
        InputStream in = this.getClass().getResourceAsStream("/en.txt");
        Scanner scanner = new Scanner(in);
        System.out.println("Dictionary is loading...");
        while (scanner.hasNext()) {
            for (String word : scanner.nextLine().toLowerCase().split("\\W+")) {
                dict.putIfAbsent(word, 1);
                dict.put(word, dict.get(word) + 1);
            }
        }
    }

    double probability(String word) {
        return (double) dict.getOrDefault("word", 1) / totalCounter;
    }

    Set<String> known(Set<String> words) {
        return words.stream().filter(dict::containsKey).collect(Collectors.toSet());
    }

    Set<String> edit1(String word) {
        Set<String> candidates = new HashSet<>();
        candidates.addAll(delete(word));
        candidates.addAll(insert(word));
        candidates.addAll(replace(word));
        candidates.addAll(transpose(word));
        return candidates;
    }

    Set<String> edit2(String word) {
        return edit1(word).stream().map(this::edit1).flatMap(Set::stream).collect(Collectors.toSet());
    }

    Set<String> delete(String word) {
        Set<String> words = new HashSet<>();
        for (int i = 0; i < word.length(); i++) {
            words.add(word.substring(0, i) + word.substring(i + 1));
        }
        return words;
    }

    Set<String> insert(String word) {
        Set<String> words = new HashSet<>();
        for (int i = 0; i < word.length(); i++) {
            for (Character c : alphabet.toCharArray()) {
                words.add(word.substring(0, i) + c + word.substring(i));
            }
        }
        return words;
    }

    Set<String> replace(String word) {
        Set<String> words = new HashSet<>();
        for (int i = 0; i < word.length(); i++) {
            for (Character c : alphabet.toCharArray()) {
                words.add(word.substring(0, i) + c + word.substring(i + 1));
            }
        }
        return words;
    }

    Set<String> transpose(String word) {
        Set<String> words = new HashSet<>();
        for (int i = 1; i < word.length(); i++) {
            words.add(word.substring(0, i - 1) + word.charAt(i) + word.charAt(i - 1) + word.substring(i + 1));
        }
        return words;
    }


    String correction(String word) {
        if (dict.containsKey(word)) {
            return word;
        }
        Set<String> edit1 = known(edit1(word));
        if (!edit1.isEmpty()) {
            return edit1.stream().max(Comparator.comparingInt(dict::get)).orElseThrow(RuntimeException::new);
        }

        Set<String> edit2 = known(edit2(word));
        if (!edit2.isEmpty()) {
            return edit2.stream().max(Comparator.comparingInt(dict::get)).orElseThrow(RuntimeException::new);
        }

        return word;
    }

    public enum Lang {RU, EN}
}
