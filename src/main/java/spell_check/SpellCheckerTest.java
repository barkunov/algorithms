package spell_check;

import java.io.BufferedReader;
import java.io.Console;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

public class SpellCheckerTest {
    public static void main(String[] args) throws IOException {
        SpellChecker spellChecker = new SpellChecker(SpellChecker.Lang.RU);
        String input = "";
        System.out.println("Для выхода нажмите 9");
        BufferedReader reader =
                new BufferedReader(new InputStreamReader(System.in));
        while (!input.equals("9")) {
            System.out.print("Введите слово для проверки: ");
            input = reader.readLine();
            System.out.println("Видимо, вы имели в виду: " + spellChecker.correction(input));
        }
    }
}
