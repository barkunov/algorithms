package lib_new;

import java.util.Objects;

public class BTree<T extends Comparable<T>> {
    private Node<T> top = null;

    public Node<T> getTop() {
        return top;
    }

    public BTree<T> add(T value) {
        Node<T> novel = new Node<>(value);
        if (top == null) {
            this.top = novel;
        } else {
            add(top, novel);
        }
        return this;
    }

    private void add(Node<T> current, Node<T> novel) {
        if (current.compareTo(novel) > 0 && current.left == null) {
            current.left = novel;
        } else if (current.compareTo(novel) < 0 && current.right == null) {
            current.right = novel;
        } else if (current.compareTo(novel) > 0) {
            add(current.left, novel);
        } else {
            add(current.right, novel);
        }
    }


    public static class Node<T extends Comparable<T>> implements Comparable<Node<T>> {
        final T value;
        public Node<T> left = null;
        public Node<T> right = null;

        public Node(T value) {
            this.value = value;
        }

        @Override
        public int compareTo(Node<T> other) {
            return value.compareTo(other.value);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Node<?> node = (Node<?>) o;
            return value.equals(node.value);
        }

        @Override
        public int hashCode() {
            return Objects.hash(value);
        }

        @Override
        public String toString() {
            return "Node{" +
                    "value=" + value +
                    '}';
        }
    }
}
