package lib_new;

import java.util.*;
import java.util.stream.Collectors;

public class Trie {
    private final TrieNode root = new TrieNode(null);
    private final Character closingChar;

    public Trie(Character closingChar) {
        this.closingChar = closingChar;
    }

    public void add(String word) {
        add(root, word + closingChar, 0);
    }

    private void add(final TrieNode node, final String word, final int index) {
        if (index == word.length()) {
            return;
        }
        Character c = word.charAt(index);
        node.kids.computeIfAbsent(c, TrieNode::new);
        add(node.kids.get(c), word, index + 1);

    }

    public boolean exist(final String word) {
        return exist(root, word + closingChar, 0);
    }

    private boolean exist(TrieNode node, String word, int index) {
        if (index == word.length()) {
            return true;
        } else if (!node.kids.containsKey(word.charAt(index))) {
            return false;
        } else {
            return exist(node.kids.get(word.charAt(index)), word, index + 1);
        }
    }

    public Set<String> autocomplete(final String prefix) {
        Optional<TrieNode> startingNode = findStartingNode(root, prefix, 0);
        return startingNode
                .map(this::autocomplete)
                .orElse(Collections.emptySet())
                .stream()
                .map(w -> w.substring(1, w.length() - 1))
                .map(w -> prefix + w)
                .collect(Collectors.toSet());
    }

    private Optional<TrieNode> findStartingNode(TrieNode node, String prefix, int index) {
        if (index == prefix.length()) {
            return Optional.of(node);
        } else if (!node.kids.containsKey(prefix.charAt(index))) {
            return Optional.empty();
        } else {
            return findStartingNode(node.kids.get(prefix.charAt(index)), prefix, index + 1);
        }
    }

    private Set<String> autocomplete(TrieNode node) {
        Set<String> words = new HashSet<>();
        for (TrieNode child : node.kids.values()) {
            if (!child.value.equals(this.closingChar)) {
                words.addAll(autocomplete(child));
            } else {
                words.add(String.valueOf(closingChar));
            }
        }
        return words.stream()
                .map(w -> node.value + w)
                .collect(Collectors.toSet());
    }

    public static class TrieNode {
        final Character value;
        final Map<Character, TrieNode> kids = new HashMap<>();

        public TrieNode(final Character value) {
            this.value = value;
        }
    }
}
