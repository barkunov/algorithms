package lib.model;

/**
 * @author Kirill Barkunov on 23.10.17.
 */
public interface TreeNode<T extends Comparable> {
    T getValue();
    TreeNode<T> getLeft();
    TreeNode<T> getRight();
    TreeNode<T> getParent();

    void setLeft(TreeNode<T> node);
    void setRight(TreeNode<T> node);
    void setParent(TreeNode<T> node);
}
