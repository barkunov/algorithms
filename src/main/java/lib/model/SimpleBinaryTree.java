package lib.model;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * @author Kirill Barkunov on 23.10.17.
 */
public class SimpleBinaryTree<T extends Comparable<T>> implements BinaryTree<T> {


    private SimpleTreeNode<T> top = null;

    @Override
    public TreeNode add(T value) {
        if (top == null) {
            top = new SimpleTreeNode<>(value, null);
            return top;
        }

        return add(top, value);
    }

    private TreeNode add(SimpleTreeNode<T> node, T value) {
        if (node.getValue().compareTo(value) == 0) {
            return node;
        } else if (value.compareTo(node.getValue()) < 0) {
            if (node.getLeft() == null) {
                node.setLeft(new SimpleTreeNode<>(value, node));
                return node.getLeft();
            } else {
                return add(node.getLeft(), value);
            }
        } else {
            if (node.getRight() == null) {
                node.setRight(new SimpleTreeNode<>(value, node));
                return node.getRight();
            } else {
                return add(node.getRight(), value);
            }
        }
    }

    @Override
    public TreeNode find(T value) {
        throw new NotImplementedException();
    }

    @Override
    public boolean remove(T value) {
        throw new NotImplementedException();
    }

    @Override
    public void print() {
        printByLevels();
    }

    public void printByLevels() {
        SortedMap<Integer, List<TreeNode>> levels = new TreeMap<>();
        fillLevels(top, levels, 1);

        levels.entrySet().forEach(entry -> {
            System.out.println("Level " + entry.getKey() + ":");
            entry.getValue().forEach(node -> {
                System.out.print(node + ", ");
            });
            System.out.println();
        });
    }

    private void fillLevels(SimpleTreeNode<T> node, SortedMap<Integer, List<TreeNode>> levels, int currentLevel) {
        if (node == null) {
            return;
        }
        addLevel(levels, currentLevel, node);

        fillLevels(node.getLeft(), levels, currentLevel + 1);
        fillLevels(node.getRight(), levels, currentLevel + 1);
    }

    private void addLevel(SortedMap<Integer, List<TreeNode>> levels, int currentLevel, SimpleTreeNode<T> node) {
        List<TreeNode> level = levels.get(currentLevel);

        if (level == null) {
            level = new ArrayList<>();
            levels.put(currentLevel, level);
        }

        level.add(node);
    }

    public void printSorted(SimpleTreeNode<T> node) {
        if (node == null) {
            return;
        }

        if (node.getLeft() != null) {
            printSorted(node.getLeft());
        }

        System.out.println(node.getValue());

        if (node.getRight() != null) {
            printSorted(node.getRight());
        }
    }

    @Override
    public List<TreeNode> findPath(T source, T destination) {
        throw new NotImplementedException();
    }
}
