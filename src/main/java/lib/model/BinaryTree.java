package lib.model;

import java.util.List;

/**
 * @author Kirill Barkunov on 23.10.17.
 */
public interface BinaryTree<T extends Comparable<T>> {
    TreeNode add(T value);
    TreeNode find(T value);
    boolean remove(T value);
    void print();

    List<TreeNode> findPath(T source, T destination);
}
