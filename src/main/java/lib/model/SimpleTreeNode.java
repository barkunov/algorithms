package lib.model;

/**
 * @author Kirill Barkunov on 23.10.17.
 */
public class SimpleTreeNode<T extends Comparable<T>> implements TreeNode<T> {

    private T value;
    private SimpleTreeNode<T> left = null;
    private SimpleTreeNode<T> right = null;
    private SimpleTreeNode<T> parent = null;

    public SimpleTreeNode(T value, SimpleTreeNode<T> parent) {
        this.value = value;
        this.parent = parent;
    }

    public SimpleTreeNode(T value) {
        this.value = value;
    }

    @Override
    public T getValue() {
        return value;
    }

    @Override
    public SimpleTreeNode<T> getLeft() {
        return left;
    }

    @Override
    public SimpleTreeNode<T> getRight() {
        return right;
    }

    @Override
    public SimpleTreeNode<T> getParent() {
        return parent;
    }

    @Override
    public void setLeft(TreeNode<T> node) {
        this.left = (SimpleTreeNode<T>) node;
    }

    @Override
    public void setRight(TreeNode<T> node) {
        this.right = (SimpleTreeNode<T>) node;
    }

    @Override
    public void setParent(TreeNode<T> node) {
        this.parent = (SimpleTreeNode<T>) node;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }
}
