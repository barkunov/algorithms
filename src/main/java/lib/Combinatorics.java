package lib;

/**
 * @author Kirill Barkunov on 7.11.17.
 */
public class Combinatorics {
    public static long factorial(long number) {
        long result = 1;
        while (number != 0) {
            result *= number;
            number--;
        }

        return result;
    }

    public static long arrangementWithoutRepetitions(long source, long base) {
        return factorial(source) / factorial(source - base);
    }

    public static long arrangementWithRepetitions(long source, long base) {
        return (long) Math.pow(source, base);
    }

    public static long combinationWithoutRepetitions(long source, long base) {
        return arrangementWithoutRepetitions(source, base) / factorial(base);
    }

    public static long combinationWithRepetitions(long source, long base) {
        return combinationWithoutRepetitions(source + base - 1, base);
    }


}
