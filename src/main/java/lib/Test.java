package lib;

import lib.model.BinaryTree;
import lib.model.SimpleBinaryTree;

/**
 * @author Kirill Barkunov on 23.10.17.
 */
public class Test {

    public static void main(String[] args) {
        BinaryTree<Integer> tree = new SimpleBinaryTree<>();

        tree.add(1);
        tree.add(7);
        tree.add(2);
        tree.add(-100);
        tree.add(3);
        tree.add(0);

        tree.print();
    }
}
