package learn;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Kirill Barkunov on 22.10.17.
 */

public class Test {


    public static void main(String[] args) {

        BTree tree = new BTree();


//        tree.add(9);
//        tree.add(7);
//        tree.add(3);
//        tree.add(5);
//        tree.add(1);

        tree.add(5);
        tree.add(6);
        tree.add(3);
        tree.add(1);
        tree.add(2);
        tree.add(4);

        int dist = tree.findDist(2, 4);

        System.out.println(dist);
        System.out.println("hello");
    }


}

class BTree {

    Node top = null;

    public void add(int value) {
        if (top == null) {
            this.top = new Node(value, null);
        } else {
            add(top, value);
        }
    }

    public int findDist(int source, int dest) {
        if (dest < source) {
            int temp = source;
            source = dest;
            dest = temp;
        }
        Node sourceNode = find(top, source);
        Node destNode = find(top, dest);

        if ( sourceNode == null || destNode == null) {
            return -1;
        }

        return findDist(sourceNode, destNode);
    }

    private int findDist(Node sourceNode, Node destNode) {
        List<Node> path = new ArrayList<>();
        int result = search(sourceNode, destNode, path);
        Node temp = sourceNode;
        List<Node> tempPath = new ArrayList<>(path);
        while (result == -1) {
            if (temp.parent != null) {

                tempPath.add(temp.parent);
                result = search(temp.parent, destNode, tempPath);
                temp = temp.parent;
            } else {
                return -1;
            }
        }
        return result;
    }

    private int search(Node currentNode, Node destNode, List<Node> path) {
        if (currentNode == null) {
            return -1;
        }

        if (currentNode.value == destNode.value) {
            System.out.println(path);
            return path.size();
        }

        List<Node> childPath = new ArrayList<>(path);

        Node nextChildNode;
        if (currentNode.value > destNode.value) {
            childPath.add(currentNode.left);
            nextChildNode = currentNode.left;
        } else {
            childPath.add(currentNode.right);
            nextChildNode = currentNode.right;
        }

        return search(nextChildNode, destNode, childPath);

    }

    private int childSearch(Node currentNode, Node destNode, List<Node> path) {
        return 0;
    }

    public Node find(int value) {
        return find(top, value);
    }

    private Node find(Node node, int value) {
        if (node == null) {
            return null;
        }

        if (node.value == value) {
            return node;
        }

        if (node.value < value) {
            return find(node.right, value);
        } else {
            return find(node.left, value);
        }
    }

    private void add(Node node, int value) {
        if (node.value == value) {
            return;
        }

        if (value > node.value) {
            if (node.right != null) {
                add(node.right, value);
            } else {
                node.right = new Node(value, node);
                return;
            }
        }

        if (value < node.value) {
            if (node.left != null) {
                add(node.left, value);
            } else {
                node.left = new Node(value, node);
                return;
            }
        }
    }


    class Node {
        int value;
        Node left = null;
        Node right = null;
        Node parent = null;

        public Node(int value, Node parent) {
            this.value = value;
            this.parent = parent;
        }

        @Override
        public String toString() {
            return String.valueOf(value);
        }
    }
}