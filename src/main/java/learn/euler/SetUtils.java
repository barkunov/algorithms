package learn.euler;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Kirill Barkunov on 20.10.17.
 */
public class SetUtils {

    public static <T> Set<Set<T>> buildPowerSet(Set<T> inputSet) {
        Set<Set<T>> result = new HashSet<>();
        if (inputSet == null || inputSet.isEmpty()) {
            result.add(new HashSet<>());
            return result;
        }
        List<T> list = new ArrayList<>(inputSet);
        T first = list.get(0);
        Set<T> tail = new HashSet<>(list.subList(1, list.size()));
        for (Set<T> set : buildPowerSet(tail)) {
            if (set.size() <= 5) {
                Set<T> newSet = new HashSet<>();
                newSet.addAll(set);
                newSet.add(first);
                result.add(set);
                result.add(newSet);
            }
        }
        return result;
    }
}
