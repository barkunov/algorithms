package learn.euler.problem21;

import java.util.HashMap;
import java.util.SortedSet;
import java.util.TreeSet;

import static learn.euler.NumberUtils.getProperDivisors;
import static learn.euler.NumberUtils.sum;

/**
 * @author Kirill Barkunov on 20.10.17.
 */
public class Problem21 {

    public static void main(String... args) {
        Integer input = 10000;
        HashMap<Integer,Integer> divisorsSum = new HashMap<>(input);
        SortedSet<Integer> friendNumbers = new TreeSet<>();

        for (int i=1; i<=input; i++) {
            divisorsSum.put(i, sum(getProperDivisors(i)));
        }

        divisorsSum.entrySet().forEach(entry -> {
            if ((!entry.getKey().equals(entry.getValue())) &&(divisorsSum.containsKey(entry.getValue()))) {
                if (divisorsSum.get(entry.getValue()).equals(entry.getKey())) {
                    friendNumbers.add(entry.getKey());
                    friendNumbers.add(entry.getValue());
                }
            }

        });

        System.out.println(friendNumbers);

        System.out.println(sum(friendNumbers));


    }
}
