package learn.euler.problem1;

import learn.euler.NumberUtils;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * @author Kirill Barkunov on 20.10.17.
 */
public class Problem1 {

    public static void main(String... args) {
        Integer input = 1000;

        SortedSet<Integer> results = new TreeSet<>();
        HashMap<Integer, List<Integer>> temp = new HashMap<Integer, List<Integer>>() {
            @Override
            public String toString() {
                StringBuilder sb = new StringBuilder();
                Set<Entry<Integer, List<Integer>>> entries = entrySet();
                for (Entry entry: entries) {
                    sb.append(entry.getKey());
                    sb.append(" - ");
                    sb.append(entry.getValue().toString());
                    sb.append('\n');
                }
                return sb.toString();
            }
        };

        for (int i=2; i<input; i++) {
            List<Integer> divisors = NumberUtils.getPrimeDivisors((long) i);
            temp.put(i, divisors);
            if (containsOnly3And5(divisors)) {
                results.add(i);
            }
        }

        System.out.println(results);
        System.out.println(NumberUtils.sum(results));
        System.out.println(temp);
    }

    public static void main1(String... args) {
        NumberUtils.getPrimeDivisors((long) 3);
    }


    public static boolean containsOnly3And5(Collection<Integer> numbers) {
        Set<Integer> copy = new HashSet<>(numbers);
        copy.remove(3);
        copy.remove(5);
        return copy.size() == 1;
    }
}
