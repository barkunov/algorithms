package learn.euler.problem1;

import learn.euler.NumberUtils;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * @author Kirill Barkunov on 20.10.17.
 */
public class Problem1Real {

    public static void main(String... args) {
        Integer input = 1000;

        SortedSet<Integer> results = new TreeSet<>();
        HashMap<Integer, List<Integer>> temp = new HashMap<>();

        for (int i=2; i<input; i++) {
            List<Integer> divisors = NumberUtils.getPrimeDivisors((long) i);
            temp.put(i, divisors);
            if (contains3or5(divisors)) {
                results.add(i);
            }
        }

        System.out.println(results);
        System.out.println(NumberUtils.sum(results));
        System.out.println(temp);
    }

    public static void main1(String... args) {
        NumberUtils.getPrimeDivisors((long) 3);
    }


    private static boolean contains3or5(Collection<Integer> numbers) {
        return numbers.contains(3) || numbers.contains(5);
    }
}
