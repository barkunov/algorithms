package learn.euler.problem60;

import learn.euler.PowerSet;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import static learn.euler.NumberUtils.getNextPrime;
import static learn.euler.NumberUtils.isPrime;
import static learn.euler.SetUtils.buildPowerSet;

/**
 * @author Kirill Barkunov on 20.10.17.
 */
public class Problem60 {

    static Set<Set<Long>> checked = new HashSet<>();

    public static void main(String[] args) {
//        Set<Long> input = new HashSet<>(Arrays.asList(3L, 7L, 109L, 673L));

        Set<Long> primes = new HashSet<>();

        final boolean[] found = {false};
        long i = 1;

        while (!found[0]) {
            Long nextPrime = getNextPrime(i);
            primes.add(nextPrime);
//            System.out.println(primes);

            if (primes.size() >= 5) {

                //Set<Set<Long>> primesBy5 = getSetOfSubsets(primes, 4);
                PowerSet<Long> powerSet = new PowerSet<>(primes, 5, 5);
                while (powerSet.hasNext()) {
                    Set<Long> set = powerSet.next();
                    if (checked.contains(set)) {
                        continue;
                    }
                    if (solvesProblem60(set)) {
                        found[0] = true;
                        System.out.println(set);
                    } else {
                        checked.add(set);
                    }
                }
            }

            i = nextPrime + 1;
        }

        System.out.println("end");

//        System.out.println(solvesProblem60(input));


    }


    public static boolean solvesProblem60(Set<Long> input) {
        PowerSet<Long> powerSet = new PowerSet<>(input, 2, 2);
//        Set<Set<Long>> setOfSubsets = getSetOfSubsets(input, 2);

        final boolean[] result = {true};
        while (powerSet.hasNext()) {
            Set<Long> set = powerSet.next();
            if (!checkConcatPrimes(set)) {
                result[0] = false;
            }
        }

        return result[0];
    }

    public static boolean checkConcatPrimes(Set<Long> set) {
        if (set == null || set.size() < 2) {
            return false;
        }
        Iterator<Long> iterator = set.iterator();

        Long a = iterator.next();
        Long b = iterator.next();

        return checkConcatPrimes(a, b);
    }

    public static boolean checkConcatPrimes(Long a, Long b) {
        if (!isPrime(a) || !isPrime(b)) {
            return false;
        }

        String sA = String.valueOf(a);
        String sB = String.valueOf(b);

        Long concat1 = Long.valueOf(sA + sB);
        Long concat2 = Long.valueOf(sB + sA);

        if (!isPrime(concat1)) {
            return false;
        }

        if (!isPrime(concat2)) {
            return false;
        }

        return true;
    }

    public static Set<Set<Long>> getSetOfSubsets(Set<Long> set, int factor) {
        Set<Set<Long>> result = new HashSet<>();
        Set<Set<Long>> powerSet = buildPowerSet(set);

        powerSet.forEach(entry -> {
            if (entry.size() == factor) {
                result.add(entry);
            }
        });

        return result;
    }


}
