package learn.euler;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Kirill Barkunov on 20.10.17.
 */
public class TimeTest {

    public static void main1(String[] args) {
        long start = System.currentTimeMillis();
        List<Integer> primeDivisors = NumberUtils.getPrimeDivisors(100000000L);
        long finish = System.currentTimeMillis();
        System.out.println((finish - start) + "millis");
        System.out.println(primeDivisors);
    }

    public static void main2(String[] args) {
        long start = System.currentTimeMillis();
        List<Integer> primeDivisors = NumberUtils.getPrimeDivisorsFast(100000000L);
        long finish = System.currentTimeMillis();
        System.out.println((finish - start) + "millis");
        System.out.println(primeDivisors);
    }

    public static void main3(String[] args) {
        Pattern patternSimple = Pattern.compile("api\\/v1\\/users\\/(.*?)\\/documents\\/(.*?)\\/frontside");
        Matcher matcherSimpleNoUUID = patternSimple.matcher("api/v1/users/{uid}/documents/{did}/frontside");
        Matcher matcherSimpleUUID = patternSimple.matcher("api/v1/users/8b213967-b4bf-423d-8518-82278b8bc0a1/documents/1114b094-2981-4fe4-ae37-f711c7d5055e/frontside");

        Pattern patternUUID = Pattern.compile("api\\/v1\\/users\\/([a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12})\\/documents\\/([a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12})\\/frontside");
        Matcher matcherUUIDNoUUID = patternUUID.matcher("api/v1/users/{uid}/documents/{did}/frontside");
        Matcher matcherUUIDUUID = patternUUID.matcher("api/v1/users/8b213967-b4bf-423d-8518-82278b8bc0a1/documents/1114b094-2981-4fe4-ae37-f711c7d5055e/frontside");

        System.out.println(matcherSimpleNoUUID.matches());
        System.out.println(matcherSimpleUUID.matches());

        System.out.println(matcherUUIDNoUUID.matches());
        System.out.println(matcherUUIDUUID.matches());
    }
    public static void main(String[] args) {
        Pattern pattern = Pattern.compile(".*api\\/v1\\/users\\/(.*)\\/documents\\/(.*)\\/frontside");
        Matcher matcher = pattern.matcher("/api/v1/users/AF0CFAFB-00AA-4A8A-90E6-FCF1863F5A57/documents/DI-DOC-B217A2F2-B867-451C-B3BC-6AEE5BF22FA1/frontside");
        System.out.println(matcher.matches());
    }
}
