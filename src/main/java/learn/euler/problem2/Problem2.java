package learn.euler.problem2;

import learn.euler.NumberUtils;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static learn.euler.NumberUtils.isFibonnacci;

/**
 * @author Kirill Barkunov on 20.10.17.
 */
public class Problem2 {

    public static void main(String... args) {
        Integer input = 4000000;

        List<Integer> fibonacci = new LinkedList<>();
        List<Integer> evenFibonacci = new LinkedList<>();

        for (int i = 0; i < input; i++) {
            if (isFibonnacci(i, fibonacci)) {
                fibonacci.add(i);
                if (i % 2 == 0) {
                    evenFibonacci.add(i);
                }
            }
        }

        System.out.println(fibonacci);
        System.out.println(evenFibonacci);
        System.out.println(NumberUtils.sum(evenFibonacci));
    }

    public static void main1(String... args) {
        System.out.println(isFibonnacci(5, Arrays.asList(1, 2, 3)));
    }
}
