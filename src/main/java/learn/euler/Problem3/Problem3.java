package learn.euler.Problem3;

import learn.euler.NumberUtils;

import java.util.List;

/**
 * @author Kirill Barkunov on 20.10.17.
 */
public class Problem3 {

    public static void main(String[] args) {
        Long input = 600851475143L;

        List<Long> primeFactors = NumberUtils.getPrimeDivisorsLongFast(input);
        System.out.println(primeFactors);
        System.out.println(primeFactors.get(primeFactors.size() - 1));
    }

    public static void main2(String[] args) {
        Long input = 600851475143L;
        Long biggestPrimeDivisor = NumberUtils.findBiggestPrimeDivisor(input);
        System.out.println(biggestPrimeDivisor);
    }


    public static void main3(String[] args) {
        Long input = 14L;
        Long biggestPrimeDivisor = NumberUtils.findBiggestPrimeDivisor(input);
        System.out.println(biggestPrimeDivisor);
    }

    public static void main4(String[] args) {
        List<Long> list = NumberUtils.getSequanceOfPrimeNumbersUntil(100000L);
        System.out.println(list);
    }
}
