package learn.euler;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Kirill Barkunov on 20.10.17.
 */
public class NumberUtils {

    private static ArrayList<Long> primes = new ArrayList<>();

    public static Integer sum(Collection<Integer> numbers) {
        return numbers.stream().mapToInt(Integer::intValue).sum();
    }

    public static List<Integer> getProperDivisors(Integer number) {
        ArrayList<Integer> divisors = new ArrayList<>();
        for (int i = 1; i < number; i++) {
            if (number % i == 0) {
                divisors.add(i);
            }
        }

        return divisors;
    }

    public static List<Integer> getDivisors(Integer number) {
        ArrayList<Integer> divisors = new ArrayList<>();
        for (int i = 1; i <= number; i++) {
            if (number % i == 0) {
                divisors.add(i);
            }
        }

        return divisors;
    }

    public static List<Long> getDivisorsLong(Long number) {
        ArrayList<Long> divisors = new ArrayList<>();
        for (long i = 1; i <= number; i++) {
            if (number % i == 0) {
                divisors.add(i);
            }
        }

        return divisors;
    }

    public static List<Integer> getPrimeDivisors(Long number) {
        ArrayList<Integer> divisors = new ArrayList<>();
        for (int i = 1; i <= number; i++) {
            if (number % i == 0) {
                if (i == 1 || getDivisors(i).size() == 2) {
                    divisors.add(i);
                }
            }
        }

        return divisors;
    }

    public static List<Long> getPrimeDivisorsLong(Long number) {
        ArrayList<Long> divisors = new ArrayList<>();
        for (long i = 1; i <= number; i++) {
            if (number % i == 0) {
                if (i == 1 || getDivisorsLong(i).size() == 2) {
                    divisors.add(i);
                }
            }
        }

        return divisors;
    }

    public static boolean isFibonnacci(Integer number, List<Integer> lesserFibonaccis) {
        if (number < 1) {
            return false;
        } else if (number == 1) {
            return true;
        } else if (number == 2) {
            return true;
        } else {
            return number == (lesserFibonaccis.get(lesserFibonaccis.size() - 1)
                    + (lesserFibonaccis.get(lesserFibonaccis.size() - 2)));
        }
    }

    public static Long findBiggestPrimeDivisor(Long number) {

        for (long i = number; i>0; i--) {
            if (i % 100000000 == 0) {
                System.out.println(i);
            }
            if (number % i == 0) {
                if (isPrime(i)) {
                    return i;
                }
            }
        }
         return -1L;
    }

    public static boolean isPrime(Long number) {
        if (primes.contains(number)) {
            return true;
        }

        if (number == 1) {
            primes.add(number);
            return true;
        }

        if (number % 2 == 0) {
            if (number == 2){
                primes.add(number);
                return true;
            } else {
                return false;
            }
        }

        for (long i = 3; i < number; i = i + 2) {
            if (number % i == 0) {
                return false;
            }
        }

        primes.add(number);
        return true;
    }

    public static Long getNextPrime(Long number) {
        while(!isPrime(number)) {
            number++;
        }
        return number;
    }

    public static List<Long> getSequanceOfPrimeNumbersUntil(Long number) {
        ArrayList<Long> result = new ArrayList<>();

        for (long i=1; i<=number; i++) {
            if (i % 100 == 0) {
                System.out.println(i);
            }
            if (isPrime(i)) {
                result.add(i);
            }
        }

        return result;
    }

    public static List<Integer> getPrimeDivisorsFast(long number) {
        ArrayList<Integer> divisors = new ArrayList<>();
        long end = number;
        for (long i  = 1; i <= end; i++) {
            if (number % i == 0) {
                if (i == 1 || getDivisors((int) i).size() == 2) {
                    divisors.add((int) i);
                    end = end / i;
                }
            }
        }

        return divisors;
    }

    public static List<Long> getPrimeDivisorsLongFast(Long number) {
        ArrayList<Long> divisors = new ArrayList<>();
        long end = number;
        for (long i = 1; i <= end; i++) {
            if (i % 10000000 == 0) {
                System.out.println(i);
            }
            if (number % i == 0) {
                if (i == 1 || getDivisorsLong(i).size() == 2) {
                    divisors.add(i);
                    end = end / i;
                    System.out.println("added: " + i);
                    System.out.println("new end: " + end);
                }
            }
        }

        return divisors;
    }
}
