package learn.algs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Kirill Barkunov on 18.10.17.
 */
public class ShellSort {


    public static void main(String... args) {
        List<Integer> numbers = Arrays.asList(4, 3, 7, 5, 3, 8, 3, 6, 9, 3, 6, 8, 6, 4, 34, 5, 7);
        System.out.println(numbers);
        List<Integer> sortedNumbers = insertionSort(numbers);
        System.out.println(sortedNumbers);
        List<Integer> sortedNumbers2 = insertionSort2(numbers);
        System.out.println(sortedNumbers2);
        int[] sortedShell = shellSort(toIntArray(numbers));

    }

    private static List<Integer> insertionSort(List<Integer> numbers) {
        ArrayList<Integer> sorted = new ArrayList<Integer>(numbers.size());

        sorted.add(numbers.get(0));

        for (int i = 1; i < numbers.size(); i++) {
            for (int j = 0; j < sorted.size(); j++) {
                if (sorted.get(j) >= numbers.get(i)) {
                    sorted.add(j, numbers.get(i));
                    break;
                } else {
                    if (j == sorted.size() - 1) {
                        sorted.add(sorted.size(), numbers.get(i));
                        break;
                    }
                }
            }
        }

        return sorted;
    }

    private static List<Integer> insertionSort2(List<Integer> numbers) {
        ArrayList<Integer> sorted = new ArrayList<Integer>(numbers);


        for (int i = 1; i < sorted.size(); i++) {
            int j = i;
            while (j > 0) {
                if (sorted.get(j) < sorted.get(j - 1)) {
                    int temp = sorted.get(j);
                    sorted.set(j, sorted.get(j - 1));
                    sorted.set(j - 1, temp);
                }
                j--;
            }
        }

        return sorted;
    }

    private static int[] shellSort(int[] A) {
        int i, j, k;
        int t;
        for (k = A.length / 2; k > 0; k /= 2)
            for (i = k; i < A.length; i++) {
                t = A[i];
                for (j = i; j >= k; j -= k) {
                    if (t < A[j - k]) {
                        A[j] = A[j - k];
                    } else {
                        break;
                    }
                }
                A[i] = t;
            }
        return A;
    }

    static int[] toIntArray(List<Integer> list)  {
        int[] ret = new int[list.size()];
        int i = 0;
        for (Integer e : list)
            ret[i++] = e.intValue();
        return ret;
    }


}
