import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

/**
 * @author Kirill Barkunov on 1.11.17.
 */
public class WaitNotifyTest {

    @Test
    public void testSynchronized() {
        Object monitor = new Object();

        ExecutorService executorService = Executors.newFixedThreadPool(8);

        List<MonitorUser> monitorUsers = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            monitorUsers.add(new MonitorUser(monitor, i));
        }

        List<Future> futures = monitorUsers.stream()
                .map(executorService::submit)
                .collect(Collectors.toList());

        System.out.println("all started!!!");

        futures.forEach(future -> {
            try {
                future.get();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        });
    }

    @Test
    public void testIllegalMonitor() throws InterruptedException {

        Message msg = new Message("process it");
        Waiter waiter = new Waiter(msg);
        new Thread(waiter, "waiter").start();

        Waiter waiter1 = new Waiter(msg);
        new Thread(waiter1, "waiter1").start();

        synchronized (msg) {
            System.out.println("CAught msg!");
        }

//        Notifier notifier = new Notifier(msg);
//        new Thread(notifier, "notifier").start();
//        System.out.println("All the threads are started");

    }

    @Test
    public void testReentrantLock() {
        List<Runnable> tasks = new ArrayList<>();
        ExecutorService executorService = Executors.newFixedThreadPool(8);
        ReentrantLock lock = new ReentrantLock(true);

        for (int i = 1; i <= 100; i++) {
            int finalI = i;
            tasks.add(() -> {
//                System.out.println(finalI + "is waiting for lock");
                lock.lock();
                System.out.println(finalI + "is in charge!");
                try {
                    Thread.sleep(5L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                lock.unlock();
            });
        }

        List<Future> futures = tasks.stream()
                .map(executorService::submit)
                .collect(Collectors.toList());

        System.out.println("all started!!!");

        futures.forEach(future -> {
            try {
                future.get();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        });
    }


    @Test
    public void testFairReentrantLock() {
        List<Runnable> tasks = new ArrayList<>();
        ExecutorService executorService = Executors.newFixedThreadPool(8);
        ReentrantLock lock = new ReentrantLock(false);

        for (int i = 1; i <= 100; i++) {
            int finalI = i;
            tasks.add(() -> {
//                System.out.println(finalI + "is waiting for lock");
                lock.lock();
                System.out.println(finalI + "is in charge!");
                try {
                    Thread.sleep(5L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                lock.unlock();
            });
        }

        List<Future> futures = tasks.stream()
                .map(executorService::submit)
                .collect(Collectors.toList());

        System.out.println("all started!!!");

        futures.forEach(future -> {
            try {
                future.get();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        });
    }


}

class Waiter implements Runnable {

    private Message msg;

    Waiter(Message m) {
        this.msg = m;
    }

    @Override
    public void run() {
        String name = Thread.currentThread().getName();
        synchronized (msg) {
            try {
                System.out.println(name + " waiting to get notified at time:" + System.currentTimeMillis());
                msg.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(name + " waiter thread got notified at time:" + System.currentTimeMillis());
            //process the message now
            System.out.println(name + " processed: " + msg.getMsg());
        }
    }

}

class Notifier implements Runnable {

    private Message msg;

    public Notifier(Message msg) {
        this.msg = msg;
    }

    @Override
    public void run() {
        String name = Thread.currentThread().getName();
        System.out.println(name + " started");
        try {
            Thread.sleep(1000);
            synchronized (msg) {
                msg.setMsg(name + " Notifier work done");
                msg.notify();
                // msg.notifyAll();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

}

class Message {
    private String msg;

    Message(String str) {
        this.msg = str;
    }

    String getMsg() {
        return msg;
    }

    void setMsg(String str) {
        this.msg = str;
    }

}

class InfiniteMonitorHolder implements Runnable {

    private final Object monitor;

    InfiniteMonitorHolder(Object monitor) {

        this.monitor = monitor;
    }

    @Override
    public void run() {
        System.out.println("sleep");
        try {
            Thread.sleep(3000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("wake up");

        monitor.notifyAll();
    }
}

class MonitorUser implements Runnable {

    private final Object monitor;
    private int num;

    MonitorUser(Object monitor, int num) {

        this.monitor = monitor;
        this.num = num;
    }

    @Override
    public void run() {
        synchronized (monitor) {
            System.out.println("Win: " + num);
            try {
                Thread.sleep(500L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
