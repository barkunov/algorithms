package lib_new;

import com.google.common.collect.ImmutableSet;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.*;
import static org.junit.jupiter.api.Assertions.*;

class TrieTest {

    @Test
    void exist() {
        // given
        Trie trie = new Trie('$');
        trie.add("kirill");
        trie.add("is");
        trie.add("a");
        trie.add("good");
        trie.add("boy");

        // when then
        assertThat(trie.exist("kirill"), is(true));
        assertThat(trie.exist("katya"), is(false));
    }

    @Test
    void autocomplete() {
        // given
        Trie trie = new Trie('$');
        trie.add("ball");
        trie.add("head");
        trie.add("pineapple");
        trie.add("hell");
        trie.add("pen");
        trie.add("heart");
        trie.add("horror");

        // when
        Set<String> hAutocomplete = trie.autocomplete("h");
        Set<String> heAutocomplete = trie.autocomplete("he");
        Set<String> heaAutocomplete = trie.autocomplete("hea");

        // then
        assertThat(hAutocomplete, is(ImmutableSet.of("head", "heart", "horror", "hell")));
        assertThat(heAutocomplete, is(ImmutableSet.of("head", "heart", "hell")));
        assertThat(heaAutocomplete, is(ImmutableSet.of("head", "heart")));
    }


    @Test
    void autocompleteEmpty() {
        // given
        Trie trie = new Trie('$');

        // when
        Set<String> autocomplete = trie.autocomplete("h");

        // then
        assertThat(autocomplete, is(Collections.emptySet()));

    }

    @Test
    void autocompleteFulWord() {
        // given
        Trie trie = new Trie('$');
        trie.add("pineapple");
        trie.add("pine");
        trie.add("head");

        // when
        Set<String> autocomplete = trie.autocomplete("pineapple");

        // then
        assertThat(autocomplete, is(ImmutableSet.of("pineapple")));
    }

    @Test
    void autocompleteNoMatch() {
        // given
        Trie trie = new Trie('$');
        trie.add("ball");
        trie.add("head");
        trie.add("pineapple");
        trie.add("hell");
        trie.add("pen");
        trie.add("heart");
        trie.add("horror");

        // when
        Set<String> autocomplete = trie.autocomplete("not_found");

        // then
        assertThat(autocomplete, is(Collections.emptySet()));
    }
}