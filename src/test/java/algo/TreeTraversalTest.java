package algo;

import com.google.common.collect.Lists;
import lib_new.BTree;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class TreeTraversalTest {

    @Test
    void breadthFirstTraverse() {
        // given
        BTree<Integer> tree = new BTree<>();
        tree.add(100);
        tree.add(90);
        tree.add(110);
        tree.add(80);

        List<BTree.Node<Integer>> expected = Lists.newArrayList(
                new BTree.Node<>(100),
                new BTree.Node<>(90),
                new BTree.Node<>(110),
                new BTree.Node<>(80)
        );

        // when
        List<BTree.Node<Integer>> actual = TreeTraversal.breadthFirstTraverse(tree);

        // then
        assertThat(actual, is(expected));
    }

    @Test
    void depthFirstTraverse() {
        // given
        BTree<Integer> tree = new BTree<>();
        tree.add(100);
        tree.add(90);
        tree.add(110);
        tree.add(80);

        List<BTree.Node<Integer>> expected = Lists.newArrayList(
                new BTree.Node<>(100),
                new BTree.Node<>(90),
                new BTree.Node<>(110),
                new BTree.Node<>(80)
        );

        // when
        List<BTree.Node<Integer>> actual = TreeTraversal.depthFirstTraverse(tree);

        // then
        assertThat(actual, is(expected));
    }
}