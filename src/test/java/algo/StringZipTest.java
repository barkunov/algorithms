package algo;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.is;

class StringZipTest {

    @Test
    void unzip_1() {
        // given
        String zip = "3[abc]4[ab]c";
        String expectedUnzip = "abcabcabcababababc";

        // when
        String actualUnzip = StringZip.unzip(zip);

        // then
        assertThat(actualUnzip, is(expectedUnzip));
    }

    @Test
    void unzip_2() {
        // given
        String zip = "10[a]";
        String expectedUnzip = "aaaaaaaaaa";

        // when
        String actualUnzip = StringZip.unzip(zip);

        // then
        assertThat(actualUnzip, is(expectedUnzip));
    }

    @Test
    void unzip_3() {
        // given
        String zip = "2[3[a]b]";
        String expectedUnzip = "aaabaaab";

        // when
        String actualUnzip = StringZip.unzip(zip);

        // then
        assertThat(actualUnzip, is(expectedUnzip));
    }
}