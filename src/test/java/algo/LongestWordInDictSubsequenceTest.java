package algo;

import com.google.common.collect.Lists;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class LongestWordInDictSubsequenceTest {

    @Test
    void calcTest_1() {
        // given
        ArrayList<String> words = Lists.newArrayList("able", "ale", "apple", "bale", "kangaroo");
        String baseWord = "abppplee";
        String expectedWord = "apple";

        //when then
        runTest(words, baseWord, expectedWord);
    }

    @Test
    void calcTest_2() {
        // given
        ArrayList<String> words = Lists.newArrayList("able", "ale", "apple", "abppplee", "bale", "kangaroo");
        String baseWord = "abppplee";
        String expectedWord = "abppplee";

        //when then
        runTest(words, baseWord, expectedWord);
    }

    @Test
    void calcTest_3() {
        // given
        ArrayList<String> words = Lists.newArrayList("able", "ale", "apple", "abpplee", "bale", "kangaroo");
        String baseWord = "abppplee";
        String expectedWord = "abpplee";

        //when then
        runTest(words, baseWord, expectedWord);

    }

    private void runTest(ArrayList<String> words, String baseWords, String expectedWord) {
        // when
        String actualWordBrute = LongestWordInDictSubsequence.calcBruteForce(baseWords, words);
        String actualWordIter = LongestWordInDictSubsequence.calcIterative(baseWords, words);
        String actualWordIterMap = LongestWordInDictSubsequence.calcIterativeWithIndexesMap(baseWords, words);

        // then
        assertThat(actualWordBrute, is(expectedWord));
        assertThat(actualWordIter, is(expectedWord));
        assertThat(actualWordIterMap, is(expectedWord));
    }

    @Test
    void isSubsequence() {
        // given
        String baseWord = "apple";
        String word = "app";

        // when
        boolean result = LongestWordInDictSubsequence.isSubsequenceStraight(baseWord, word);

        // then
        assertThat(result, is(true));
    }
}