package algo;

import com.google.common.collect.Lists;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.is;

class CanBalanceTest {

    @Test
    void canBalance_1() {
        // given
        List<Integer> list = Lists.newArrayList(1, 1, 1, 2, 1);

        // when then
        assertThat(CanBalance.canBalance(list), is(true));
    }

    @Test
    void canBalance_2() {
        // given
        List<Integer> list = Lists.newArrayList(2, 1, 1, 2, 1);

        // when then
        assertThat(CanBalance.canBalance(list), is(false));
    }

    @Test
    void canBalance_3() {
        // given
        List<Integer> list = Lists.newArrayList(10, 10);

        // when then
        assertThat(CanBalance.canBalance(list), is(true));
    }

    @Test
    void canBalance_4() {
        // given
        List<Integer> list = Lists.newArrayList(10, 10, 10);

        // when then
        assertThat(CanBalance.canBalance(list), is(false));
    }

    @Test
    void canBalance_5() {
        // given
        List<Integer> list = Lists.newArrayList(10, 10, 2, 2, 3, 5, 1, 1, 1, 1, 1, 1, 1, 1);

        // when then
        assertThat(CanBalance.canBalance(list), is(true));
    }

    @Test
    void canBalance_6() {
        // given
        List<Integer> list = Lists.newArrayList(1, 2, 100, 101);

        // when then
        assertThat(CanBalance.canBalance(list), is(true));
    }

    @Test
    void canBalance_7() {
        // given
        List<Integer> list = Lists.newArrayList(25, 49, 51, 105, 120, 140, 200);

        // when then
        assertThat(CanBalance.canBalance(list), is(true));
    }
}