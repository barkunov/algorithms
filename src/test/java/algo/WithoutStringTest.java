package algo;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.is;

class WithoutStringTest {

    @Test
    void withoutString_1() {
        // given
        String base = "Hello there";
        String remove = "llo";

        // when
        String actualRes = WithoutString.withoutString(base, remove);
        String actualResManual = WithoutString.withoutStringManual(base, remove);

        // then
        assertThat(actualRes, is("He there"));
        assertThat(actualResManual, is("He there"));
    }

    @Test
    void withoutString_2() {
        // given
        String base = "Hello there";
        String remove = "e";

        // when
        String actualRes = WithoutString.withoutString(base, remove);
        String actualResManual = WithoutString.withoutStringManual(base, remove);

        // then
        assertThat(actualRes, is("Hllo thr"));
        assertThat(actualResManual, is("Hllo thr"));
    }

    @Test
    void withoutString_3() {
        // given
        String base = "Hello there";
        String remove = "x";

        // when
        String actualRes = WithoutString.withoutString(base, remove);
        String actualResManual = WithoutString.withoutStringManual(base, remove);

        // then
        assertThat(actualRes, is("Hello there"));
        assertThat(actualResManual, is("Hello there"));
    }
}