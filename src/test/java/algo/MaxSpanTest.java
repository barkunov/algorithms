package algo;

import com.google.common.collect.Lists;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.is;

class MaxSpanTest {

    @Test
    void maxSpan_1() {
        // given
        List<Integer> list = Lists.newArrayList(1, 2, 1, 1, 3);

        // when
        Integer actualSpan = MaxSpan.maxSpan(list);

        // then
        assertThat(actualSpan, is(4));
    }

    @Test
    void maxSpan_2() {
        // given
        List<Integer> list = Lists.newArrayList(1, 4, 2, 1, 4, 1, 4);

        // when
        Integer actualSpan = MaxSpan.maxSpan(list);

        // then
        assertThat(actualSpan, is(6));
    }

    @Test
    void maxSpan_3() {
        // given
        List<Integer> list = Lists.newArrayList(1, 4, 2, 1, 4, 4, 4);

        // when
        Integer actualSpan = MaxSpan.maxSpan(list);

        // then
        assertThat(actualSpan, is(6));
    }
}