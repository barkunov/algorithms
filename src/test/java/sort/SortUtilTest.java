package sort;

import org.junit.Test;
import sort.algs.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import static org.junit.Assert.assertEquals;

/**
 * @author Kirill Barkunov on 26.10.17.
 */
public class SortUtilTest {

    private final int SIZE = 100000;

    @Test
    public void testJavaSort() throws Exception {
        List<Integer> input = generateList(SIZE);
        List<Integer> expected = new ArrayList<>(input);
        long start = System.currentTimeMillis();
        Collections.sort(expected);
        long finish = System.currentTimeMillis();
        System.out.println((finish - start) + "ms. " + "Algorithm: Java");
    }

    @Test
    public void testMergeSort() throws Exception {
        testSort(new MergeSort<>());
    }

    @Test
    public void testBubbleSort() throws Exception {
        testSort(new BubbleSort<>());
    }

    @Test
    public void testRealBubbleSort() throws Exception {
        testSort(new RealBubbleSort<>());
    }

    @Test
    public void testInsertionSort() throws Exception {
        testSort(new InsertionSort<>());
    }

    @Test
    public void testQuickSortSort() throws Exception {
        testSort(new QuickSort<>());
    }

    private void testSort(Sort<Integer> sort) throws Exception {
        // given
        List<Integer> input = generateList(SIZE);
        List<Integer> expected = new ArrayList<>(input);
        long start = System.currentTimeMillis();
        Collections.sort(expected);
        long finish = System.currentTimeMillis();

        // when
        input = sort.sort(input);

        // then
        assertEquals(expected, input);

        // post
        System.out.println((finish - start) + "ms. " + "Algorithm: " + sort.getClass().getSimpleName());
    }

    private List<Integer> generateList(int size) {
        Random random = new Random();
        List<Integer> list = new ArrayList<>(size);
        for (int i=0; i< size; i++) {
            list.add((int) (System.currentTimeMillis() % 1000));
        }

        return list;
    }

}