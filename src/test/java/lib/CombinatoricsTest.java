package lib;

import junit.framework.TestCase;

/**
 * @author Kirill Barkunov on 7.11.17.
 */
public class CombinatoricsTest extends TestCase {

    public void testFactorial() throws Exception {
        // given
        long input1 = 5;
        long input2 = 14;

        // when
        long result1 = Combinatorics.factorial(input1);
        long result2 = Combinatorics.factorial(input2);

        // then
        assertEquals(120L, result1);
        assertEquals(87178291200L, result2);
    }

    public void testArrangementWithoutRepetitions() throws Exception {
        //given
        long source1 = 7;
        long base1 = 5;

        long source2 = 4;
        long base2 = 4;

        // when
        long result1 = Combinatorics.arrangementWithoutRepetitions(source1, base1);
        long result2 = Combinatorics.arrangementWithoutRepetitions(source2, base2);

        // then
        assertEquals(2520L, result1);
        assertEquals(24L, result2);
    }

    public void testArrangementWithRepetitions() throws Exception {
        //given
        long source1 = 7;
        long base1 = 5;

        long source2 = 4;
        long base2 = 4;

        // when
        long result1 = Combinatorics.arrangementWithRepetitions(source1, base1);
        long result2 = Combinatorics.arrangementWithRepetitions(source2, base2);

        // then
        assertEquals(16807L, result1);
        assertEquals(256L, result2);
    }

    public void testCombinationWithoutRepetitions() throws Exception {
        //given
        long source1 = 7;
        long base1 = 5;

        long source2 = 4;
        long base2 = 4;

        // when
        long result1 = Combinatorics.combinationWithoutRepetitions(source1, base1);
        long result2 = Combinatorics.combinationWithoutRepetitions(source2, base2);

        // then
        assertEquals(21L, result1);
        assertEquals(1L, result2);
    }

    public void testCombinationWithRepetitions() throws Exception {
        //given
        long source1 = 7;
        long base1 = 5;

        long source2 = 4;
        long base2 = 4;

        // when
        long result1 = Combinatorics.combinationWithRepetitions(source1, base1);
        long result2 = Combinatorics.combinationWithRepetitions(source2, base2);

        // then
        assertEquals(462L, result1);
        assertEquals(35L, result2);
    }

}